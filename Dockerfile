# SPDX-FileCopyrightText: 2020-2024 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

FROM ubuntu:jammy
LABEL maintainer="Mark Jonas <toertel@gmail.com>"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && \
    apt-get -y install \
        bc \
        bison \
        build-essential \
        device-tree-compiler \
        flex \
        git \
        gcc-arm-none-eabi \
        libgnutls28-dev \
        libncurses5-dev \
        libssl-dev \
        lzop \
        perl \
        uuid-dev \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh  /

WORKDIR /mnt

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/bin/bash"]
