<!--
SPDX-FileCopyrightText: 2020-2021 Mark Jonas <toertel@gmail.com>

SPDX-License-Identifier: MIT
-->

[![Pipeline status](https://gitlab.com/toertel/docker-image-u-boot-contribute/badges/master/pipeline.svg)](https://gitlab.com/toertel/docker-image-u-boot-contribute/pipelines)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/toertel/docker-image-u-boot-contribute)](https://api.reuse.software/info/gitlab.com/toertel/docker-image-u-boot-contribute)

# Docker container for U-Boot contribution

Build the image using Docker or pull from Docker Hub.

```
docker build -t toertel/u-boot-contribute:latest .

          or

docker pull toertel/u-boot-contribute:latest
```

Clone the U-Boot source repository and run the container from within it.

```
git clone https://gitlab.denx.de/u-boot/u-boot.git
cd u-boot
docker run -it --rm -v "$PWD:/mnt" "toertel/u-boot-contribute:latest"
```

Then, inside the running container, do the following to build U-Boot.

```
export ARCH=arm
export CROSS_COMPILE=arm-none-eabi-
make mx6sxsabreauto_defconfig
make -j6
```

## Details

### The working directory

The default working directory of the container is /mnt. The idea is that you
bind-mount your U-Boot source directory to this directory.

### User and group used

When the container starts it creates a user with the same user and group ID of
the owner of the working directory. It then switches to that user. This makes
sure that the files created by the compilation process are owned by the owner
of the U-Boot source directory.

By default the name of the user is *user*. You can change it by setting the
container environment variable `USERNAME`.

```
docker run -it --rm -e USERNAME=u-boot -v "$PWD:/mnt" "toertel/u-boot-contribute:latest"
```

### Overriding the default command

The default command executed by the container is `/bin/bash`. You can override
it by appending it to the command line to start the container. The following
command starts the container, builds U-Boot, and terminates.

```
docker run -it --rm -v "$PWD:/mnt" "toertel/u-boot-contribute:latest" bash -c "export ARCH=arm; export CROSS_COMPILE=arm-none-eabi-; make mx6sxsabreauto_defconfig; make -j6"
```
