#!/bin/sh

# SPDX-FileCopyrightText: 2021 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

: "${USER_NAME:=dev}"
: "${GROUP_NAME:=devs}"

# Get uid and gid from current directory
DIR_UID=$(stat --format="%u" .)
DIR_GID=$(stat --format="%g" .)

WITH_USER=1

# Create group except for GID 0
if [ "$DIR_GID" -eq 0 ]; then
    GROUP_NAME=$(id -gn 0)
    echo "Using group $GROUP_NAME (gid 0)"
else
    if groupadd -g "$DIR_GID" "$GROUP_NAME"; then
        echo "Using group $GROUP_NAME (gid $DIR_GID)"
    else
        WITH_USER=0
    fi
fi

# Create user except for UID 0 and assign group
if [ "$WITH_USER" -eq 1 ]; then
    if [ "$DIR_UID" -eq 0 ]; then
        USER_NAME=$(id -un 0)
        usermod -g "$GROUP_NAME" "$USER_NAME"
        echo "Using user $USER_NAME (uid 0)"
    else
        if useradd -u "$DIR_UID" -g "$GROUP_NAME" "$USER_NAME"; then
            echo "Using user $USER_NAME (uid $DIR_UID)"
        else
            WITH_USER=0
        fi
    fi
fi

# Start Docker command
if [ "$WITH_USER" -eq 1 ]; then
    echo "Switching to $USER_NAME"
    id "$USER_NAME"
    exec runuser -u "$USER_NAME" -- "$@"
else
    echo "Skipping user switch, running with default user"
    "$@"
fi
